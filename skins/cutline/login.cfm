<cfimport prefix="mango" taglib="../../tags/mango">
<cfimport prefix="mangox" taglib="../../tags/mangoextras">
<cfimport prefix="template" taglib=".">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="text/html; charset=<mango:Blog charset />" />
	<title>Login &#8212; <mango:Blog title /></title>
	
	<meta name="generator" content="Mango <mango:Blog version />" />
	
	<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/custom.css" type="text/css" media="screen" />
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie7.css" media="screen" />
	<![endif]-->
	<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie6.css" media="screen" />
	<![endif]-->
	
	<meta name="robots" content="noindex, nofollow" />
	<mango:Event name="beforeHtmlHeadEnd" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="./js/navigation.js"></script>
</head>
<body class="custom" style="margin-top:23px;">

<div class="nav-cont">
    <div class="nav-bg"></div>
    <div id="page-container">
    <cfinclude template="cfincludes/hme_logo.cfm">
    <ul id="nav">
    
            <mango:Pages><mango:Page>
                <!---<li><a href="<mango:PageProperty link>" title="<mango:PageProperty title />">
                    <mango:PageProperty title /></a></li>--->
                                </mango:Page></mango:Pages>
                                
                                <!---<li class="rss"><a href="<mango:Blog rssurl />">RSS</a></li>
                                <li><a class="current" href="http://www.hme.com">return to website</a></li>
                                <li><a class="current" href="<mango:Blog basePath />">HME QSR Blog</a></li>--->
        </ul>
    </div>
</div>

<mango:Event name="beforeHtmlBodyStart" />

<div id="header_img" style="width:100%;">
    <div style="width:100%;">
        <div class="header_img_cont" style="background:url('<mango:Blog skinurl />assets/images/Blog_Banner_Desktop2.jpg');background-repeat:no-repeat;background-size:cover;">
            <div id="container">
    			<h1>DRIVE-THRU<br />HEADSETS &amp;<br />TIMER SYSTEMS</h1>
            </div>
        </div>
    </div>
</div>

<div id="container">
	<!---<div id="masthead">
		<h1><a href="<mango:Blog url />"><mango:Blog title /></a></h1>
		<h3><mango:Blog tagline /></h3>
	</div>

	<ul id="nav">
	</ul>--->
	
	<!---<div id="header_img">
		<img src="<mango:Blog skinurl />assets/images/header_1.png" width="930" />
	</div>--->
    
    <!-- Image Swap <div id="header_img">
		<img src="<mango:Blog skinurl />assets/images/header_4.jpg" width="770" height="140" />
	</div> -->
	<div id="content_box">
		<div id="content" class="pages">
		
		<h2>Login</h2>
		<div class="entry">
			<mango:RequestVar ifExists="errormsg">
				<p class="error"><mango:RequestVar name="errormsg" /></p>
			</mango:RequestVar>
		<cfoutput><form action="<mangox:Environment selfUrl />" method="post" id="login_form"></cfoutput>
	
				<label for="username">Username:</label>
				<p>
				<input name="username" id="username" value="" size="22" type="text" class="text_input">
				</p>
				<p>
				<label for="password">Password:</label><br />
				<input name="password" id="password" value="" size="22" type="password" class="text_input">
				</p>
				<input name="login" class="form_submit" type="submit" id="submit" 
						src="<mango:Blog skinurl />assets/images/submit_comment.gif" value="Login" />
			</form>
		
		</div>
		<div class="clear"></div>

		</div>

	</div>

	<div id="footer"><mango:Event name="afterFooterStart" />
		<p><mango:Blog title /> &mdash; <a href="http://www.mangoblog.org" title="Mango Blog - A ColdFusion blog engine">Powered by Mango Blog</a> &mdash; Design by <a href="http://www.tubetorial.com">Chris Pearson</a> ported by <a href="http://www.asfusion.com">AsFusion</a></p>
	<mango:Event name="beforeFooterEnd" />
	</div>
</div>
<mango:Event name="beforeHtmlBodyEnd" />
</body>
</html>
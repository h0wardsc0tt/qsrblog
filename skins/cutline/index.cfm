<cfimport prefix="mango" taglib="../../tags/mango">
<cfimport prefix="mangox" taglib="../../tags/mangoextras">
<cfimport prefix="template" taglib=".">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="text/html; charset=<mango:Blog charset />" />
	<title><mango:Blog title /> &#8212; <mango:Blog tagline /></title>
	<meta name="generator" content="Mango <mango:Blog version />" />
	<meta name="description" content="<mango:Blog description />" />
	<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<mango:Blog skinurl />assets/styles/custom.css" type="text/css" media="screen" />
	<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie7.css" media="screen" />
	<![endif]-->
	<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="<mango:Blog skinurl />assets/styles/ie6.css" media="screen" />
	<![endif]-->
	<meta name="robots" content="index, follow" />
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<mango:Blog rssurl />" />
	<link rel="alternate" type="application/atom+xml" title="Atom" href="<mango:Blog atomurl />" />	
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="<mango:Blog apiurl />" />
	<mango:Event name="beforeHtmlHeadEnd" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="./js/navigation.js"></script>
</head>

<body class="custom" style="margin-top:23px;">

<div class="nav-cont">
    <div class="nav-bg"></div>
    <div id="page-container">
    	<cfinclude template="cfincludes/hme_logo.cfm">
    	<ul id="nav">
            <mango:Pages><mango:Page>
                <!---<li><a href="<mango:PageProperty link>" title="<mango:PageProperty title />">
                    <mango:PageProperty title /></a></li>--->
            </mango:Page></mango:Pages>
            
            <li class="rss"><a href="<mango:Blog rssurl />">RSS</a></li>
            <li><a class="current" href="http://www.hme.com">return to website</a></li>
            <li><a class="current" href="<mango:Blog basePath />">HME QSR Blog</a></li>
        </ul>
    </div>
</div>
<mango:Event name="beforeHtmlBodyStart" />

<div id="header_img" style="width:100%;">
    <div style="width:100%;">
        <div class="header_img_cont" style="background:url('<mango:Blog skinurl />assets/images/Blog_Banner_Desktop2.jpg');background-repeat:no-repeat;background-size:cover;">
            <div id="container">
    			<h1>DRIVE-THRU<br />HEADSETS &amp;<br />TIMER SYSTEMS</h1>
            </div>
        </div>
    </div>
</div>

<div id="container">

<!---	<div id="masthead">
		<h1><a href="<mango:Blog url />"><mango:Blog title /></a></h1>
		<h3><mango:Blog tagline /></h3>
	</div> 

	
	
	<div id="header_img">
    	<div>
			<img src="<mango:Blog skinurl />assets/images/header_1.png" width="930" alt="<mango:Blog title />" title="<mango:Blog title />" />
        </div>
	</div>--->
    
  <!-- Original Image Swap  <div id="header_img">
		<img src="<mango:Blog skinurl />assets/images/header_<mangox:Random items='1.jpg,2.jpg,3.jpg,4.jpg'/>" width="770" height="140" alt="<mango:Blog title />" title="<mango:Blog title />" />
	</div> -->
	<div id="content_box">
	
		<div id="content" class="posts">
		<mango:Posts count="10">
			<mango:Post>	
			<h2><a href="<mango:PostProperty link />" rel="bookmark" title="Permanent Link to <mango:PostProperty title />"><mango:PostProperty title /></a></h2>
			<h4><mango:PostProperty date dateformat="mmmm dd, yyyy" /> &middot; By <mango:PostProperty author /> &middot; <mango:PostProperty ifcommentsallowed><a href="<mango:PostProperty link />#respond" title="Comment on <mango:PostProperty title />"><mango:PostProperty ifCommentCountGT="0"><mango:PostProperty commentCount /> Comment<mango:PostProperty ifCommentCountGT="1">s</mango:PostProperty></mango:PostProperty><mango:PostProperty ifCommentCountLT="1">No Comments</mango:PostProperty></a></mango:PostProperty></h4>
			<div class="entry">
				<mango:PostProperty ifhasExcerpt excerpt>
				<p><a href="<mango:PostProperty link />" title="Read the rest of this entry">[Read more &rarr;]</a></p>
				</mango:PostProperty>
				<mango:PostProperty ifNotHasExcerpt body />
				<!-- AddThis Button BEGIN -->
				<cfsavecontent variable="postURL"><mango:PostProperty link /></cfsavecontent>
				<cfsavecontent variable="postTitle"><mango:PostProperty title /></cfsavecontent>
				<cfoutput>
				<div class="addthis_toolbox addthis_default_style " 
					addthis:url="http://#CGI.HTTP_HOST##postURL#"
					addthis:title="#postTitle#">
					<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
					<a class="addthis_button_tweet"></a>
					<a class="addthis_counter addthis_pill_style"></a>
				</div>
				</cfoutput>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50eaf3b47bb2934d"></script>
				<!-- AddThis Button END -->
			</div>
			<div class="entry-footer entry">
			<mango:Event name="beforePostContentEnd" template="index" mode="excerpt" />
			</div>
			<p class="tagged"><span class="add_comment"><mango:PostProperty ifcommentsallowed>&rarr; <a href="<mango:PostProperty link />#respond" title="Comment on <mango:PostProperty title />"><mango:PostProperty ifCommentCountGT="0"><mango:PostProperty commentCount /> Comment<mango:PostProperty ifCommentCountGT="1">s</mango:PostProperty></mango:PostProperty><mango:PostProperty ifCommentCountLT="1">No Comments</mango:PostProperty></a></mango:PostProperty></span><strong>Tags:</strong> 
			<mango:Categories><mango:Category><a href="<mango:CategoryProperty link />" title="View all posts in  <mango:CategoryProperty title />" rel="category tag"><mango:CategoryProperty title /></a> <mango:Category ifCurrentIsNotLast>&middot; </mango:Category></mango:Category></mango:Categories>
			</p>
			<div class="clear"></div>
			</mango:Post>
		</mango:Posts>
			
<mango:Archive pageSize="10">
<div class="navigation">
	<div class="previous"><mango:ArchiveProperty ifHasNextPage><a class="previous" href="<mango:ArchiveProperty link pageDifference="1" />">&larr; Previous Entries</a></mango:ArchiveProperty></div>
	<div class="next"></div>
</div>
</mango:Archive>
<div class="clear flat"></div>
</div>

<div id="sidebar">
	<ul class="sidebar_list">
		<mangox:PodGroup locationId="sidebar" template="index">
			<mangox:TemplatePod id="blog-description" title="HME QSR">
			<p><mango:Blog description descriptionParagraphFormat /></p>
			</mangox:TemplatePod>
			<template:sidebar />
		</mangox:PodGroup>	
	</ul>
</div>	
	</div>
	<div id="footer"><mango:Event name="afterFooterStart" />
		<p>&#169; <cfoutput>#Year(Now())#</cfoutput> HME, Inc.</p>
	<mango:Event name="beforeFooterEnd" />
	</div>
</div>
<mango:Event name="beforeHtmlBodyEnd" />
</body>
</html>
$(function() {
	var isiPad = navigator.userAgent.match(/iPad/i) != null;
	
	/* Fixed Navigation */
	$(window).on('scroll load resize', function() {
		console.log($('body').width());
		if( $('body').width() > 991 ) {
			if ($(this).scrollTop() > 75 || isiPad === true) { // this refers to window
				$('.nav-bg').fadeIn();
			} else {
				$('.nav-bg').fadeOut();
			}
		}  else {
			 $('.nav-bg').fadeIn();
		}
	});
});
<cfcomponent name="BlackList" hint="Create a black list to stop spam messages">

	<cfset variables.UserBlackList_IP = "" />
	<cfset variables.UserBlackList_Agent = "" />
	<cfset variables.UserBlackList_Words = "" />

	<cffunction name="init" access="public" output="false" returntype="any">
		<cfscript>
			//This is a list of users that have been determined to be sending out spam messages
			variables.UserBlackList_IP = "207.249.9.137,91.134.32.25,188.143.232.84,89.97.241.138,109.73.78.99,91.207.8.106";
			variables.UserBlackList_Agent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)|Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)|Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2b1) Gecko/20091014 Firefox/3.6b1 GTB5|Mozilla/4.0 (compatible; MSIE 6.0; America Online Browser 1.1; rev1.2; Windows NT 5.1; SV1; .NET CLR 1.1.4322)|Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; Cng)|Mozilla/5.0 (Windows NT 5.1; U; en) Opera 8.01 ";
			variables.UserBlackList_Words = "cialis,viagra,blowjob,bondage,casino,cock,dick,dildo,erection,erotic,fuck,naked,nude,penis,tits,vagina,underage,sex,shemale,fag,pussy,porn,keaijvvv,http://,declare @,varchar(,exec(";
		</cfscript>
        
		<cfreturn this />
	</cffunction>

	<!--- ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: --->
	<cffunction name="blockSpam" output="false" returnType="boolean" description="block spam messeges from being passed through forms" access="public">
        <cfargument type="struct" name="thisForm" required="true">
        <cfargument type="String" name="HTTP_USER_AGENT" required="true">
        <cfargument type="String" name="REMOTE_ADDR" required="true">
        <cfargument type="String" name="HTTP_HOST" required="true">
        <cfargument type="String" name="SCRIPT_NAME" required="true">
     
     	<cfscript>
        	var badAgent  = false;
			var thisAgent = '';
			var thisField = '';
			var thisWord  = '';
		</cfscript>
	
        <cfloop list="#variables.UserBlackList_Agent#" index="thisAgent" delimiters="|">
            <cfif FindNoCase(thisAgent, Arguments.HTTP_USER_AGENT) GT 0>
                <cfset badAgent = true />
            </cfif>
        </cfloop>
        
        <cfif REFindNoCase("[a-z0-9]", Arguments.HTTP_USER_AGENT) EQ 0>
            <cfset badAgent = true />
        </cfif>
        
        <cfloop list="#Arguments.thisForm.fieldnames#" index="thisField">
            <cfloop list="#variables.UserBlackList_Words#" index="thisWord">
                <cfif FindNoCase(thisWord, Evaluate(thisField)) GT 0>
                    <cfset badAgent = true />
                    <cfbreak />
                </cfif>
            </cfloop>
        </cfloop>
        
        <cfif ListFind(variables.UserBlackList_IP, Arguments.REMOTE_ADDR) OR badAgent>
            <cftry>
                <cfmail 
                    to="wwang@hme.com" 
                    from="no-reply@hme.com"
                    subject="Bot found on #Arguments.HTTP_HOST# - #Arguments.SCRIPT_NAME#" type="html">
                    IP: #Arguments.REMOTE_ADDR#<br />
                    Agent: #Arguments.HTTP_USER_AGENT#<br />
                    <cfif StructKeyExists(Arguments.thisForm, "fieldnames")>
                    	<cfdump var="#Arguments.thisForm#">
                    </cfif>
                </cfmail>
                <cfcatch type="any"></cfcatch>
            </cftry>
            
            <cfreturn true />
        <cfelse>
        	<cfreturn false />
        </cfif>
        
	</cffunction> 

</cfcomponent>